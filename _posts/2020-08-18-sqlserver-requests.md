---
layout: post
title: Monitoring SQL Server
date: 2020-08-18 11:25:00 -0600
description: Script I used to monitor SQL Server activities. 
img: aug-2020/torreys-peak.jpg
tags: [script, sqlserver]
---

Every SQL Server DBA has own set of scripts to monitor SQL Server. [This](https://gitlab.com/susantha.bathige/code/-/snippets/2005574){:target="_blank"} is the script I use at very first time if I need to see what is going on with any SQL Server instance. I have been using this with SQL Server 2016 to 2019. Script needs some tweaking for the prior versions of SQL Servers. 

Below is the same script for your quick reference. I commented out few lines which I uncomment only if I need to dig in more. 
```sql
SELECT 
	req.session_id,
	s.program_name,
	blk_by = wt.blocking_session_id,
	case 
		when req2.plan_handle <> req.plan_handle then req2.plan_handle
	end as actual_blk_plan_handle,
	req.status,
	req.command,
	req.wait_type,
	req.row_count,
	req.cpu_time,
	req.total_elapsed_time,
	s.login_name,
	DB_NAME(req.database_id) as dbname,
	objname = (select OBJECT_SCHEMA_NAME(objectid,req.database_id) + '.' + OBJECT_NAME(objectid,dbid) from sys.dm_exec_sql_text(req.sql_handle)),
	statement_text = (select SUBSTRING(sqltext.text, CASE req.statement_start_offset WHEN 0 THEN req.statement_start_offset+1 ELSE req.statement_start_offset/2 END
				,CASE req.statement_end_offset WHEN -1 THEN DATALENGTH(sqltext.text)/2  ELSE req.statement_end_offset/2 END) from sys.dm_exec_sql_text(req.sql_handle) sqltext ),
	time_elapsed_sec = DATEDIFF(s,req.start_time,getdate()),
	s.host_name,
	req.wait_resource,
    transaction_isolation =
        CASE s.transaction_isolation_level
            WHEN 0 THEN 'Unspecified'
            WHEN 1 THEN 'Read Uncommitted'
            WHEN 2 THEN 'Read Committed'
            WHEN 3 THEN 'Repeatable'
            WHEN 4 THEN 'Serializable'
            WHEN 5 THEN 'Snapshot'
        END,
	ConnectionWrites   = con.num_writes,
    ConnectionReads    = con.num_reads,
    ClientAddress      = con.client_net_address,
    Authentication     = con.auth_scheme,	  
	req.sql_handle,
	req2.plan_handle AS BlockedBy_handle,              
	req.plan_handle
	,query_plan = (select query_plan from sys.dm_exec_text_query_plan(req.sql_handle, req.statement_start_offset, req.statement_end_offset ))
	--,sqltext.text
	--,CAST(sqlplan.query_plan AS XML) As QueryPlan
FROM sys.dm_exec_requests req WITH (NOLOCK)
INNER JOIN sys.dm_exec_sessions s WITH (NOLOCK)
	ON req.session_id=s.session_id
LEFT JOIN sys.dm_os_waiting_tasks wt WITH (NOLOCK) 
	ON req.session_id=wt.session_id
LEFT JOIN sys.dm_exec_requests req2 WITH (NOLOCK)
	ON req.session_id=req2.blocking_session_id
LEFT JOIN sys.dm_exec_connections con
	ON con.session_id = req.session_id
where req.status  not in ('background','sleeping')
	and req.session_id <> @@spid
order by --req.session_id 
         req.cpu_time DESC
```

<em>**About the post header picture**</em>: It was taken when hiking [Torreys peak](https://en.wikipedia.org/wiki/Torreys_Peak){:target="_blank"} mountain (14,275') on July, 2020. The peak you see at far away is the summit of Torreys peak. 
